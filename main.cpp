
#include "../include/hash.h"


int main(int argc, char const *argv[]) {


   int choice;


   std::cout << R"(
                   #############################
                   #                           #
                   #                           #
                   #         HASHASH           #
                   #                           #
                   #                           #
                   #############################
                )" << "\n\n";

   std::cout << "Choose an Option: \n\n";
   std::cout << "1: Encrypt a message\n";
   std::cout << "2: Decrypt a message\n";
   std::cout << ": "; std::cin >> choice; std::cout << "\n";


   switch (choice) {
      case 1:
         cryptor();
         break;
      case 2:
         decryptor();
         break;
      default:
         std::cout << "> Error!\n\n";
         break;
   }


   return 0;
}
