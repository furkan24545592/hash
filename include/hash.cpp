#include "hash.h"


int cryptor(){

   std::string input;
   int key;
   char hash, tempHash;


   std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   std::cout << "Input the Message: "; std::getline(std::cin, input);
   std::cout << "Choose a key to hash message: "; std::cin >> key;
   std::cout << std::endl;
   std::cout << "New Hash: ";

      for (size_t i = 0; i < input.length(); i++)
      {
         tempHash = key ^ input[i];
         hash = static_cast<char>(tempHash);
         std::cout << hash;

      }
      std::cout << "\n\n";
      system("pause");

   return 0;
}


int decryptor(){

   std::string input;
   int key;
   char hash, tempHash;

   std::cout << "Input the value: "; std::cin >> input;
   std::cout << "Choose a key to hash message: "; std::cin >> key;
   std::cout << std::endl;
   std::cout << "New Text: ";

      for (size_t i = 0; i < input.length(); i++)
      {
         tempHash = key ^ input[i];
         hash = static_cast<char>(tempHash);
         std::cout << hash;

      }
      std::cout << "\n\n";
      system("pause");

   return 0;
}
